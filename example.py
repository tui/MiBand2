#!/usr/bin/python3
import sys
import time
import argparse
from datetime import datetime
from base import MiBand2
from constants import ALERT_TYPES

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--standard',  action='store_true',help='Shows device information')
parser.add_argument('-r', '--recorded',  action='store_true',help='Shows previews recorded data')
parser.add_argument('-l', '--live',  action='store_true',help='Measures live heart rate')
parser.add_argument('-y', '--my',  action='store_true',help='My test')
parser.add_argument('-i', '--init',  action='store_true',help='Initializes the device')
parser.add_argument('-m', '--mac', required=True, help='Mac address of the device')
parser.add_argument('-t', '--set_current_time', action='store_true',help='Set time')
parser.add_argument('-a', '--alert-message', action='store_true',help='Send message alert to watch')
parser.add_argument('--alert-phone', action='store_true',help='Send phone alert to watch')
parser.add_argument('--alert-vibrate', action='store_true',help='Vibrate the watch')
args = parser.parse_args()

MAC = args.mac # sys.argv[1]

band = MiBand2(MAC, debug=True)
band.setSecurityLevel(level="medium")

if args.init:
    if band.initialize():
        print("Init OK")
    band.set_heart_monitor_sleep_support(enabled=False)
    band.disconnect()
    sys.exit(0)
else:
    band.authenticate()

if args.recorded:
    print('Print previews recorded data')
    band._auth_previews_data_notif(True)
    start_time = datetime.strptime("20.04.2019 00:00", "%d.%m.%Y %H:%M")
    band.start_get_previews_data(start_time)
    while band.active:
        band.waitForNotifications(0.1)

if args.alert_message:
    band.send_alert(ALERT_TYPES.MESSAGE)
if args.alert_phone:
    band.send_alert(ALERT_TYPES.PHONE)
if args.alert_vibrate:
    band.send_alert(ALERT_TYPES.VIBRATE)
    
def test_alerts():
    print('Message notif')
    band.send_alert(ALERT_TYPES.MESSAGE)
    time.sleep(2)
    # this will vibrate till not off
    print('Phone notif')
    band.send_alert(ALERT_TYPES.PHONE)
    time.sleep(2)
    print('Vibrate only')
    band.send_alert(ALERT_TYPES.VIBRATE)
    time.sleep(2)

if args.standard:
    if False:
        test_alerts()
    print('OFF')
    band.send_alert(ALERT_TYPES.NONE)
    print('Soft revision:',band.get_revision())
    print('Hardware revision:',band.get_hrdw_revision())
    print('Serial:',band.get_serial())
    print('Battery:', band.get_battery_info())
    print('Time:', band.get_current_time())
    print('Steps:', band.get_steps())
    print('Heart rate oneshot:', band.get_heart_rate_one_time())

def test_heart():
    # I get notified when user goes through the "heart" icon
    band.send_alert(ALERT_TYPES.PHONE)    
    band._char_caller_name.write(b"Ahoj svete")    
    band._desc_auth.write(b"\x01\x00", True)

    # hrm_data', 'event', 'raw_data
    band._desc_device_event.write(b"\x01\x00", True)
    while True:
        band.waitForNotifications(0.1)

def test_icons():
    # No luck at all
    band.send_alert(b'\x01\x00')
    time.sleep(2)
    band.send_alert(b'\x01\x01')
    time.sleep(2)
    band.send_alert(b'\x01\x02')
    time.sleep(2)
    band.send_alert(b'\x01\x03')
    time.sleep(2)
    band.send_alert(b'\x01\x04')
    time.sleep(2)
    band.send_alert(b'\x01\x05')
    time.sleep(2)
    band.send_alert(b'\x01\x06')
    time.sleep(2)

def test_text():
    band._char_caller_name.write(b"\x03\x05Ahoj svete")
    band.send_alert(b"\x02\x05Ahoj svete")
#    band.send_alert(b"\x02")    
    band._desc_auth.write(b"\x01\x00", True)

    # hrm_data', 'event', 'raw_data
#    band._desc_device_event.write(b"\x01\x00", True)
    while True:
        band.waitForNotifications(0.1)
        
if args.my:
    print('My test')
    test_text()

def l(x):
    print('Realtime heart:', x)


def b(x):
    print('Raw heart:', x)


def f(x):
    print('Raw accel heart:', x)
   
if args.set_current_time:
    now = datetime.now()
    print ('Set time to:', now)
    print ('Returned: ', band.set_current_time(now))
    print ('Time:', band.get_current_time())

def l(x):
    print ('Realtime heart:', x)

def b(x):
    print ('Raw heart:', x)


def f(x):
    print ('Raw accel heart:', x)

if args.live:
    # band.start_heart_rate_realtime(heart_measure_callback=l)
    band.start_raw_data_realtime(
            heart_measure_callback=l,
            heart_raw_callback=b,
            accel_raw_callback=f)

band.disconnect()
