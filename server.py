#!/usr/bin/python3
import sys
import time
import argparse
import os
import stat
import signal
from datetime import datetime
from base import MiBand2
from constants import ALERT_TYPES

class Server:
    def periodic(m):
        print(time.time(), "Periodic check")

        m.last = time.time()

    def connect(m):
        band = MiBand2(m.mac, debug=True)
        band.setSecurityLevel(level="medium")
        band.authenticate()
        m.band = band
        
    def server_loop(m):
        m.connect()
        os.unlink("/tmp/watch")
        os.mknod("/tmp/watch", stat.S_IFIFO | 0o600)

        print("Openning comm socket")
        f = os.open("/tmp/watch", os.O_RDONLY | os.O_NDELAY)
        print("Entering main loop")

        m.periodic()
        while True:
            period = 100
            os.set_blocking(f, True)
            signal.alarm(period)
            data = os.read(f, 1024)
            m.band.waitForNotifications(0.1)
            if time.time() - m.last > period:
                m.periodic()
            if not data:
                continue
            print("Got data:", data)

            if data == b"message\n":
                print("Attempting alert")
                m.band.send_alert(ALERT_TYPES.MESSAGE)
            if data == b"phone\n":
                print("Attempting alert")
                m.band.send_alert(ALERT_TYPES.PHONE)
            if data == b"vibrate\n":
                print("Attempting alert")
                m.band.send_alert(ALERT_TYPES.VIBRATE)
        m.band.disconnect()

    def server_loop_force(m):
        while True:
            try:
                m.server_loop()
            except e:
                print(e)

def for_log():
    print('Soft revision:',band.get_revision())
    print('Hardware revision:',band.get_hrdw_revision())
    print('Serial:',band.get_serial())
    print('Battery:', band.get_battery_info())
    print('Time:', band.get_current_time())
    print('Steps:', band.get_steps())
    print('Heart rate oneshot:', band.get_heart_rate_one_time())

parser = argparse.ArgumentParser()
parser.add_argument('-m', '--mac', required=True, help='Mac address of the device')
args = parser.parse_args()

s = Server()
s.mac = args.mac
s.server_loop_force()
